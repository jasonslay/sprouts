import argparse
import os
import sys
import time
import logging
import requests
import traceback

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import WebDriverWait


logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")


def send_pushover_message(title, message, image=None):
    app_token = os.environ.get("PUSHOVER_APP_TOKEN")
    user_key = os.environ.get("PUSHOVER_USER_KEY")
    if app_token and user_key:
        if image:
            files = {"attachment": (os.path.basename(image), open(image, "rb"), "image/png")}
        else:
            files = {}
        requests.post(
            "https://api.pushover.net/1/messages.json",
            data={"token": app_token, "user": user_key, "title": title, "message": message},
            files=files,
        )


def clip_coupons(username, password, debug_mode=False):
    options = webdriver.ChromeOptions()
    if not debug_mode:
        options.add_argument("headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--lang=en-US")
    options.add_argument("--window-size=1920x1080")
    browser = webdriver.Chrome("/usr/local/bin/chromedriver", options=options)
    try:
        page = 1
        browser.get(f"https://shop.sprouts.com/shop/coupons?page={page}")
        WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.ID, "shopping-selector-parent-process-modal-close-click"))
        )
        browser.find_element_by_id("shopping-selector-parent-process-modal-close-click").click()
        WebDriverWait(browser, 10).until(EC.visibility_of_element_located((By.ID, "nav-register")))
        time.sleep(5)
        browser.find_element_by_id("nav-register").click()
        email_input = browser.find_element_by_name("username")
        email_input.send_keys(username)
        password_input = browser.find_element_by_name("password")
        password_input.send_keys(password)
        browser.find_element_by_id("login-submit").click()
        time.sleep(5)
        num_coupons_on_page = len(browser.find_elements_by_class_name("coupon"))
        while num_coupons_on_page:
            print(f"Clipping coupons on page {page}")
            unclipped_coupons = browser.find_elements_by_xpath('//div[text() = "Clip"]')
            logging.info(
                f"{len(browser.find_elements_by_class_name('coupon'))} Total / {len(unclipped_coupons)} Unclipped"
            )
            clipped = 0
            for coupon in [c for c in unclipped_coupons if c.is_displayed()]:
                coupon.click()
                clipped += 1
                time.sleep(0.1)
            page += 1
            browser.get(f"https://shop.sprouts.com/shop/coupons?page={page}")
            time.sleep(5)
            num_coupons_on_page = len(browser.find_elements_by_class_name("coupon"))
    except Exception:
        logging.error(traceback.format_exc())
        try:
            browser.get_screenshot_as_file("/tmp/sprouts.png")
        except WebDriverException:
            send_pushover_message(message=traceback.format_exc(), title="Error!")
        else:
            send_pushover_message(message=traceback.format_exc(), title="Error!", image="/tmp/sprouts.png")
    else:
        logging.info("Added {} new coupons".format(clipped))
        if clipped:
            send_pushover_message(message="Added {} new coupons".format(clipped), title="Added coupons!")
    browser.quit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clip coupons on sprouts.com.")
    parser.add_argument("username", help="username to use")
    parser.add_argument("password", help="password to use")
    parser.add_argument("--debug", action="store_true", help="launch broswer without headless mode")
    args = parser.parse_args()
    clip_coupons(username=args.username, password=args.password, debug_mode=args.debug)
